package main

import "fmt"

func main() {
	// 1. 打印Hello World
	fmt.Println("Hello, World!")

	// 2. 声明变量并赋值
	var message string = "Hello, Go!"
	fmt.Println(message)

	// 3. 简化的变量声明与赋值
	name := "Alice"
	fmt.Println("My name is", name)

	// 4. 整数运算
	num1 := 10
	num2 := 5
	sum := num1 + num2
	fmt.Println("Sum:", sum)

	// 5. 浮点数运算
	f1 := 3.14
	f2 := 2.5
	result := f1 * f2
	fmt.Println("Result:", result)

	// 6. 字符串拼接
	str1 := "Hello"
	str2 := "Go"
	str := str1 + " " + str2
	fmt.Println(str)

	// 7. 条件语句 - if
	age := 18
	if age >= 18 {
		fmt.Println("You are an adult")
	} else {
		fmt.Println("You are a minor")
	}

	// 8. 条件语句 - switch
	day := "Monday"
	switch day {
	case "Monday":
		fmt.Println("It's Monday!")
	case "Tuesday":
		fmt.Println("It's Tuesday!")
	default:
		fmt.Println("It's another day!")
	}

	// 9. 循环 - for
	for i := 1; i <= 5; i++ {
		fmt.Println(i)
	}

	// 10. 循环 - while (使用for实现)
	j := 1
	for j <= 5 {
		fmt.Println(j)
		j++
	}

	// 11. 循环 - 无限循环
	k := 1
	for {
		if k > 5 {
			break
		}
		fmt.Println(k)
		k++
	}

	// 12. 数组的声明与初始化
	var numbers [3]int
	numbers[0] = 1
	numbers[1] = 2
	numbers[2] = 3
	fmt.Println(numbers)

	// 13. 数组的长度
	fmt.Println("Length:", len(numbers))

	// 14. 使用数组字面量声明并初始化数组
	fruits := [3]string{"Apple", "Banana", "Orange"}
	fmt.Println(fruits)

	// 15. 切片的声明与初始化
	var colors []string
	colors = append(colors, "Red")
	colors = append(colors, "Green")
	colors = append(colors, "Blue")
	fmt.Println(colors)

	// 16. 切片的长度与容量
	fmt.Println("Length:", len(colors))
	fmt.Println("Capacity:", cap(colors))

	// 17. 使用切片字面量声明并初始化切片
	animals := []string{"Dog", "Cat", "Lion"}
	fmt.Println(animals)

	// 18. 切片的切割
	slice1 := []int{1, 2, 3, 4, 5}
	slice2 := slice1[1:3]
	fmt.Println(slice2)

	// 19. 切片的追加
	slice3 := []int{1, 2, 3}
	slice4 := []int{4, 5}
	slice5 := append(slice3, slice4...)
	fmt.Println(slice5)

	// 20. 切片的复制
	slice6 := []int{1, 2, 3}
	slice7 := make([]int, len(slice6))
	copy(slice7, slice6)
	fmt.Println(slice7)

	// 21. 函数的声明与调用
	greet()

	// 22. 函数的参数与返回值
	result := multiply(3, 4)
	fmt.Println("Result:", result)

	// 23. 多个返回值的函数
	a, b := swap(10, 20)
	fmt.Println("After swap:", a, b)

	// 24. 匿名函数
	func() {
		fmt.Println("Anonymous function")
	}()

	// 25. 闭包
	addFunc := adder(10)
	fmt.Println(addFunc(5))
	fmt.Println(addFunc(10))

	// 26. defer语句
	defer fmt.Println("Deferred statement 1")
	defer fmt.Println("Deferred statement 2")
	fmt.Println("Normal statement")

	// 27. 指针的声明与使用
	number := 42
	pointer := &number
	fmt.Println("Value:", *pointer)

	// 28. 结构体的声明与使用
	person := Person{Name: "Alice", Age: 25}
	fmt.Println(person.Name, person.Age)

	// 29. 结构体的方法
	person.Introduce()

	// 30. 接口的声明与实现
	circle := Circle{Radius: 5}
	rectangle := Rectangle{Width: 3, Height: 4}
	printArea(circle)
	printArea(rectangle)

	// 31. 错误处理
	err := someFunction()
	if err != nil {
		fmt.Println("Error:", err)
	}

	// 32. 文件读写
	writeFile("data.txt", "Hello, Go!")
	content := readFile("data.txt")
	fmt.Println("File content:", content)

	// 33. 异常处理 - panic
	// panic("Something went wrong!")

	// 34. 异常处理 - recover
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered:", r)
		}
	}()
	// panic("Something went wrong!")

	// 35. JSON的编码与解码
	jsonData := `{"name": "Alice", "age": 30}`
	decodeJSON(jsonData)

	// 36. 时间操作
	currentTime := getCurrentTime()
	fmt.Println("Current time:", currentTime)

	// 37. 并发 - Goroutine
	go greet()
	go count(5)

	// 38. 并发 - 通道(Channel)
	messageCh := make(chan string)
	go sendMessage(messageCh)
	receivedMessage := <-messageCh
	fmt.Println("Received message:", receivedMessage)

	// 39. 并发 - 选择语句(Select)
	ch1 := make(chan string)
	ch2 := make(chan string)
	go sendData(ch1, "Hello")
	go sendData(ch2, "Go")
	select {
	case message := <-ch1:
		fmt.Println("Received from ch1:", message)
	case message := <-ch2:
		fmt.Println("Received from ch2:", message)
	default:
		fmt.Println("No message received")
	}

	// 40. 并发 - 互斥锁(Mutex)
	var counter int
	var mutex sync.Mutex
	for i := 0; i < 10; i++ {
		go incrementCounter(&counter, &mutex)
	}
	time.Sleep(time.Second)
	fmt.Println("Counter:", counter)

	// 41. 并发 - 条件变量(Cond)
	var isReady bool
	var cond sync.Cond = *sync.NewCond(&sync.Mutex{})
	go waitForSignal(&cond, &isReady)
	go sendSignal(&cond, &isReady)
	time.Sleep(time.Second)

	// 42. 协程间的通信 - 基于通道的消息传递
	messageCh1 := make(chan string)
	messageCh2 := make(chan string)
	go sendMessage(messageCh1)
	go receiveMessage(messageCh1, messageCh2)
	receivedMsg := <-messageCh2
	fmt.Println("Received message:", receivedMsg)

	// 43. 协程间的通信 - 基于共享内存的同步(互斥锁)
	var sharedData int
	var wg sync.WaitGroup
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go modifyData(&sharedData, &wg)
	}
	wg.Wait()
	fmt.Println("Shared data:", sharedData)

	// 44. 协程间的通信 - 基于共享内存的同步(原子操作)
	var sharedCounter int32
	var wg2 sync.WaitGroup
	for i := 0; i < 5; i++ {
		wg2.Add(1)
		go incrementCounterAtomically(&sharedCounter, &wg2)
	}
	wg2.Wait()
	fmt.Println("Shared counter:", sharedCounter)

	// 45. 延迟函数的执行 - time包
	time.AfterFunc(time.Second, func() {
		fmt.Println("Delayed function executed")
	})

	// 46. 使用标准库中的正则表达式
	match := checkEmail("test@example.com")
	fmt.Println("Email match:", match)

	// 47. 使用标准库中的排序算法
	numbersToSort := []int{5, 3, 8, 1, 2}
	sort.Ints(numbersToSort)
	fmt.Println("Sorted numbers:", numbersToSort)

	// 48. 使用标准库中的文件操作
	file, err := os.Open("data.txt")
	if err != nil {
		fmt.Println("Error:", err)
	}
	defer file.Close()
	fileInfo, err := file.Stat()
	if err != nil {
		fmt.Println("Error:", err)
	}
	fmt.Println("File size:", fileInfo.Size())

	// 49. 使用标准库中的HTTP客户端
	response, err := http.Get("https://api.example.com")
	if err != nil {
		fmt.Println("Error:", err)
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error:", err)
	}
	fmt.Println("Response body:", string(body))

	// 50. 使用标准库中的HTTP服务器
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, Go!")
	})
	http.ListenAndServe(":8080", nil)
}

// 函数示例
func greet() {
	fmt.Println("Hello, Go!")
}

func multiply(a, b int) int {
	return a * b
}

func swap(a, b int) (int, int) {
	return b, a
}

// 闭包示例
func adder(a int) func(int) int {
	return func(b int) int {
		return a + b
	}
}

// 结构体示例
type Person struct {
	Name string
	Age  int
}

func (p Person) Introduce() {
	fmt.Println("My name is", p.Name)
}

// 接口示例
type Shape interface {
	Area() float64
}

type Circle struct {
	Radius float64
}

func (c Circle) Area() float64 {
	return 3.14 * c.Radius * c.Radius
}

type Rectangle struct {
	Width  float64
	Height float64
}

func (r Rectangle) Area() float64 {
	return r.Width * r.Height
}

func printArea(s Shape) {
	fmt.Println("Area:", s.Area())
}

// 错误处理示例
func someFunction() error {
	return fmt.Errorf("Something went wrong")
}

// 文件读写示例
func writeFile(filename, content string) {
	file, err := os.Create(filename)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer file.Close()
	_, err = file.WriteString(content)
	if err != nil {
		fmt.Println("Error:", err)
	}
}

func readFile(filename string) string {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println("Error:", err)
		return ""
	}
	return string(content)
}

// JSON编码与解码示例
type Person struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func decodeJSON(jsonData string) {
	var person Person
	err := json.Unmarshal([]byte(jsonData), &person)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	fmt.Println("Name:", person.Name)
	fmt.Println("Age:", person.Age)
}

// 时间操作示例
func getCurrentTime() time.Time {
	return time.Now()
}

// 并发 - Goroutine示例
func greet() {
	fmt.Println("Hello, Go!")
}

func count(n int) {
	for i := 1; i <= n; i++ {
		fmt.Println(i)
		time.Sleep(time.Second)
	}
}

// 并发 - 通道(Channel)示例
func sendMessage(ch chan<- string) {
	ch <- "Hello, Go!"
}

// 并发 - 通道(Channel)示例
func receiveMessage(ch <-chan string, resultCh chan<- string) {
	message := <-ch
	resultCh <- message
}

// 并发 - 条件变量(Cond)示例
func waitForSignal(cond *sync.Cond, isReady *bool) {
	cond.L.Lock()
	for !*isReady {
		cond.Wait()
	}
	fmt.Println("Signal received")
	cond.L.Unlock()
}

func sendSignal(cond *sync.Cond, isReady *bool) {
	time.Sleep(2 * time.Second)
	cond.L.Lock()
	*isReady = true
	cond.Signal()
	cond.L.Unlock()
}

// 并发 - 互斥锁(Mutex)示例
func incrementCounter(counter *int, mutex *sync.Mutex) {
	mutex.Lock()
	*counter++
	mutex.Unlock()
}

// 并发 - 协程间的通信示例
func modifyData(data *int, wg *sync.WaitGroup) {
	defer wg.Done()
	for i := 0; i < 1000; i++ {
		*data++
	}
}

// 并发 - 协程间的通信示例
func incrementCounterAtomically(counter *int32, wg *sync.WaitGroup) {
	defer wg.Done()
	for i := 0; i < 1000; i++ {
		atomic.AddInt32(counter, 1)
	}
}

// 正则表达式示例
func checkEmail(email string) bool {
	pattern := `^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$`
	match, _ := regexp.MatchString(pattern, email)
	return match
}

// HTTP服务器示例
func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, Go!")
	})
	http.ListenAndServe(":8080", nil)
}
